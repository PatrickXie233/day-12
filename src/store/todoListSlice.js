import { createSlice } from '@reduxjs/toolkit'

export const todoListSlice = createSlice({
    name: 'todoListSlice',
    initialState: {
        todoList: [],
    },
    reducers: {
        setTodoList: (state, { payload }) => {
            state.todoList = payload
        },
    },
})

// Action creators are generated for each case reducer function
export const { setTodoList } = todoListSlice.actions

export default todoListSlice.reducer
