import { useState } from 'react'
import './index.css'
import { Button, Input, message } from 'antd'
import useTodos from '../../Hooks/useTodos'
const TodoGenerator = () => {
    // dependency
    const [inputValue, setInputValue] = useState('')
    const todos = useTodos()
    const [messageApi, contextHolder] = message.useMessage()
    // method
    const onClick = async () => {
        if (inputValue.length === 0) {
            alert('请输入内容')
            return
        }
        await todos.addTodoListItemHook(inputValue)
        messageApi.open({
            key: 'updatable',
            type: 'success',
            content: 'add todo item success',
        })
        setInputValue('')
    }
    const onChange = (e) => {
        setInputValue(e.target.value)
    }
    return (
        <div className="todoGenerator">
            {contextHolder}
            <Input
                className="Input"
                type="text"
                value={inputValue}
                onChange={onChange}
                placeholder="请输入Todo"
            ></Input>
            <Button onClick={onClick}>add</Button>
        </div>
    )
}

export default TodoGenerator
