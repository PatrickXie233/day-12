import { useSelector } from 'react-redux'
import { List, Card, message, Modal, Input } from 'antd'
import {
    DeleteOutlined,
    CheckCircleOutlined,
    EditOutlined,
} from '@ant-design/icons'
import './index.css'
import useTodos from '../../Hooks/useTodos.js'
import { useState } from 'react'

const TodoGroup = (props) => {
    const todoList = useSelector((state) => state.todoList.todoList)
    const todos = useTodos()
    const [messageApi, contextHolder] = message.useMessage()
    const [modalOpen, setModalOpen] = useState(false)
    const [editId, setEditId] = useState(0)
    const [editText, setEditText] = useState('')
    const [editDone, setEditDone] = useState(false)

    const handleDone = ({ id, done, text }) => {
        todos.putTodoListItemHook(id, {
            done: !done,
            text: text,
        })
        messageApi.open({
            key: 'updatable',
            type: 'success',
            content: 'update success',
        })
    }
    const handleDelete = async (id) => {
        await todos.deleteTodoListItemHook(id)
        messageApi.open({
            key: 'updatable',
            type: 'success',
            content: 'delete todo item success',
        })
    }
    const handleEdit = (id, text, done) => {
        setModalOpen(true)
        setEditDone(done)
        setEditText(text)
        setEditId(id)
    }
    const onEditTextChange = ({ target }) => {
        setEditText(target.value)
    }
    const handleTextChangeConfirm = async () => {
        await todos.putTodoListItemHook(editId, {
            text: editText,
            done: editDone,
        })
        messageApi.open({
            key: 'updatable',
            type: 'success',
            content: 'update success',
        })
        setModalOpen(false)
    }
    return (
        <div className="">
            {contextHolder}
            <List
                itemLayout="vertical"
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 2,
                    md: 4,
                    lg: 4,
                    xl: 6,
                    xxl: 4,
                }}
                dataSource={todoList}
                renderItem={(item, index) => (
                    <List.Item>
                        <Card
                            className="card"
                            title="Todo Item"
                            extra={
                                <span>
                                    <EditOutlined
                                        className="icon"
                                        onClick={() =>
                                            handleEdit(
                                                item.id,
                                                item.text,
                                                item.done,
                                            )
                                        }
                                    />
                                    <DeleteOutlined
                                        className="margin-left icon"
                                        onClick={() => handleDelete(item.id)}
                                    />
                                </span>
                            }
                        >
                            <div className="context">
                                <span
                                    className={
                                        item.done ? 'text line-through' : 'text'
                                    }
                                >
                                    {item.text}
                                </span>
                                <CheckCircleOutlined
                                    onClick={() => handleDone(item)}
                                    className={
                                        item.done
                                            ? 'finish-icon icon float'
                                            : 'icon float'
                                    }
                                />
                            </div>
                        </Card>
                    </List.Item>
                )}
            />
            <Modal
                title="update todo text"
                centered
                open={modalOpen}
                onOk={() => handleTextChangeConfirm()}
                onCancel={() => setModalOpen(false)}
            >
                <div className="modal">
                    <Input value={editText} onChange={onEditTextChange}></Input>
                </div>
            </Modal>
        </div>
    )
}
export default TodoGroup
