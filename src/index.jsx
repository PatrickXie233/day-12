import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import { Provider } from 'react-redux'
import store from './store/store'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import Help from './pages/Help'
import NotFind from './pages/NotFind/index'
import DoneListPage from './pages/DoneListPage/index'
import TodoList from './components/TodoList'
import DoneDetail from './pages/DoneDetial/index'

const router = createBrowserRouter([
    {
        path: '/',
        element: <App></App>,
        children: [
            {
                index: true,
                element: <TodoList></TodoList>,
            },
            {
                path: '/donelist',
                element: <DoneListPage></DoneListPage>,
            },
            {
                path: '/help',
                element: <Help></Help>,
            },
            {
                path: '/donelist/:id',
                element: <DoneDetail></DoneDetail>,
            },
        ],
    },
    {
        path: '*',
        element: <NotFind></NotFind>,
    },
])

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <Provider store={store}>
        <RouterProvider router={router}></RouterProvider>
    </Provider>,
)
