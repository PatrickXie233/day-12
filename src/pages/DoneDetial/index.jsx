import { Card } from 'antd'
import { useParams } from 'react-router-dom'
import './index.css'
import { useEffect, useState } from 'react'
import useTodos from '../../Hooks/useTodos'

const DoneDetail = () => {
    const { id } = useParams()
    const [todoDetail, setTodoDetail] = useState({})
    const todos = useTodos()

    useEffect(() => {
        const getTodoItem = async () => {
            const todoDetail = await todos.getTodoListByIdHook(id)
            setTodoDetail(todoDetail)
        }
        getTodoItem()
    }, [id, todos])

    return (
        <div className="card-container">
            <Card className="card">
                <div className="row">
                    <div className="row-name">id:</div>
                    <div className="row-value">{todoDetail.id}</div>
                </div>
                <div className="row">
                    <div className="row-name">text:</div>
                    <div className="row-value">{todoDetail.text}</div>
                </div>
            </Card>
        </div>
    )
}
export default DoneDetail
